#include <glog/logging.h>
#include <leveldb/db.h>
#include <lmdb.h>
#include <stdint.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <algorithm>
#include <string>


#include "caffe/proto/caffe.pb.h"
#include "caffe/util/io.hpp"

using namespace std;

string type2str(int type) {
  string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
  case CV_8U:  r = "8U"; break;
  case CV_8S:  r = "8S"; break;
  case CV_16U: r = "16U"; break;
  case CV_16S: r = "16S"; break;
  case CV_32S: r = "32S"; break;
  case CV_32F: r = "32F"; break;
  case CV_64F: r = "64F"; break;
  default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

int main(int argc, char** argv) {
  
  if (argc != 3) {
    LOG(ERROR) << "Usage: compute_image_mean <video file list> <output file>";
    return 1;
  }

  const int height = 240, width = 320; // Almost all videos in UCF101 are 240x320.
  long int count = 0;
  cv::Mat sum_frames = cv::Mat::zeros(height, width, CV_32FC3); // This is a 3 channel float image.  Set this explicitly.
  LOG(ERROR) << "Accumulating to Mat with dims " << sum_frames.rows << "," << sum_frames.cols << "," << type2str(sum_frames.type());
  cv::Mat this_frame; // This is a 3 channel unsigned char image.

  // Read in list of video files, open and accumulate sum of frames.  
  std::ifstream infile(argv[1]);
  string filename;
  int label;
  while (infile >> filename >> label) {
    cv::VideoCapture vid(filename.c_str());
    if (!vid.isOpened()) {
      LOG(ERROR) << "Could not open " << filename << "; skipping";
      continue;
    }
    LOG(ERROR) << "Processing " << filename;
    int num_frames = 0;
    while (true) {
      vid >> this_frame;
      if (!this_frame.data) break;
      
      // Four videos in UCF101 had odd dimensions. 
      if (this_frame.rows != height || this_frame.cols != width) {
	LOG(ERROR) << "Skipping " << filename << "; odd dimensions";
	break;
      }

      count++;
      num_frames++;

      // Element-wise add
      for (int i = 0; i < sum_frames.rows; i++) {
	for (int j = 0; j < sum_frames.cols; j++) {
	  for (int c = 0; c < 3; c++) {
	    sum_frames.at<cv::Vec3f>(i,j)[c] += static_cast<float>(this_frame.at<cv::Vec3b>(i,j)[c]);
	  }
	}
      }
      
    }
  }

  // Compute mean pixels.
  for (int i = 0; i < sum_frames.rows; i++) {
    for (int j = 0; j < sum_frames.cols; j++) {
      for (int c = 0; c < 3; c++) {
	sum_frames.at<cv::Vec3f>(i,j)[c] = sum_frames.at<cv::Vec3f>(i,j)[c] / count;
      }
    }
  }

  // Write out to file.  
  std::vector<int> imwrite_params;
  imwrite_params.push_back(CV_IMWRITE_JPEG_QUALITY);
  imwrite_params.push_back(100);
  cv::imwrite(argv[2], sum_frames, imwrite_params);

  return 0;
}
