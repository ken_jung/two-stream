#include <stdio.h>  // for snprintf
#include <string>
#include <vector>

#include "boost/algorithm/string.hpp"
#include "google/protobuf/text_format.h"
#include "leveldb/db.h"
#include "leveldb/write_batch.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/net.hpp"
#include "caffe/proto/caffe.pb.h"
#include "caffe/util/io.hpp"
#include "caffe/vision_layers.hpp"

using namespace caffe;  // NOLINT(build/namespaces)

template<typename Dtype>
int feature_extraction_pipeline(int argc, char** argv);

int main(int argc, char** argv) {
  return feature_extraction_pipeline<float>(argc, argv);
//  return feature_extraction_pipeline<double>(argc, argv);
}

template<typename Dtype>
int feature_extraction_pipeline(int argc, char** argv) {
  ::google::InitGoogleLogging(argv[0]);
  const int num_required_args = 6;
  if (argc < num_required_args) {
    LOG(ERROR)<<
    "This program takes in a trained spatial network and an input data layer, and then"
    " extract features of the input data produced by the net.\n"
    "Usage: test_spatial_net  <pretrained model binary>"
    "  <test proto file>  <extract_feature_blob_name1[,name2,...]>"
    "  <test file list>  <num frames per video> "
    "Note: you can extract multiple features in one pass by specifying"
    " multiple feature blob names and leveldb names seperated by ','."
    " The names cannot contain white space characters and the number of blobs"
    " and leveldbs must be equal.";
    return 1;
  }

  for (int i = 0; i < argc; i++) 
    LOG(ERROR) << i << ": " << argv[i];

  Caffe::SetDevice(0);
  Caffe::set_mode(Caffe::GPU);
  Caffe::set_phase(Caffe::TEST);

  string pretrained_binary_proto(argv[1]);
  string feature_extraction_proto(argv[2]);
  LOG(ERROR) << "Getting net definition from " << feature_extraction_proto;
  shared_ptr<Net<Dtype> > feature_extraction_net(
      new Net<Dtype>(feature_extraction_proto));
  LOG(ERROR) << "Instantiated net...";
  LOG(ERROR) << "Copying parameters from " << pretrained_binary_proto;
  feature_extraction_net->CopyTrainedLayersFrom(pretrained_binary_proto);
  LOG(ERROR) << "Loaded parameters... " << pretrained_binary_proto;

  string extract_feature_blob_names(argv[3]);
  vector<string> blob_names;
  boost::split(blob_names, extract_feature_blob_names, boost::is_any_of(","));
  size_t num_features = blob_names.size();

  for (size_t i = 0; i < num_features; i++) {
    CHECK(feature_extraction_net->has_blob(blob_names[i]))
        << "Unknown feature blob name " << blob_names[i]
        << " in the network " << feature_extraction_proto;
  }
  LOG(ERROR) << "Passed output blob name check";

  // Read in list test files.
  string test_file_list(argv[4]);
  vector<string> test_files;
  vector<int> test_labels;
  std::ifstream infile(test_file_list.c_str());
  string filename; 
  int label;
  while (infile >> filename >> label) {
    test_files.push_back(filename);
    test_labels.push_back(label);
  }
  int num_test_files = test_files.size();
  LOG(ERROR) << "Read " << num_test_files << " test files from " << test_file_list;

  int num_frames_per_video = atoi(argv[5]);
  LOG(ERROR) << "Expecting " << num_frames_per_video << " frames per video";

  int num_iters = num_test_files * num_frames_per_video;
  LOG(ERROR) << "Running for " << num_iters << " iterations";

  LOG(ERROR)<< "Extacting Features";
  vector<Blob<float>*> input_vec;
  for (int iter_index = 0; iter_index < num_iters; ++iter_index) {
    int file_index = iter_index / num_frames_per_video;
    int label = test_labels[file_index];
    int frame_num = iter_index % num_frames_per_video + 1;
    string filename = test_files[file_index];
    feature_extraction_net->Forward(input_vec); 
    for (int i = 0; i < num_features; ++i) {
      const shared_ptr<Blob<Dtype> > feature_blob = feature_extraction_net
          ->blob_by_name(blob_names[i]);
      //LOG(ERROR) << "Feature blob has: " << feature_blob->num() << ", " << feature_blob->channels() << ", " << feature_blob->height() << ", " << feature_blob->width();
      int batch_size = feature_blob->num(); // Should always be 10... 
      int dim_features = feature_blob->count() / batch_size;
      Dtype* feature_blob_data;
      for (int n = 0; n < batch_size; ++n) {
        feature_blob_data = feature_blob->mutable_cpu_data() +
            feature_blob->offset(n);
    	std::cout << filename << "\t" << label << "\t" << blob_names[i] << "\t" << frame_num << "\t" << n;
        for (int d = 0; d < dim_features; ++d) {
    	  std::cout << "\t" << feature_blob_data[d];
        }
    	std::cout << "\n";
      }  // for (int n = 0; n < batch_size; ++n)
    }  // for (int i = 0; i < num_features; ++i)
  }  // for (int iter_index = 0; iter_index < num_iters; ++iter_index)
  // write the last batch

  LOG(ERROR)<< "Successfully extracted the features!";
  return 0;
}

