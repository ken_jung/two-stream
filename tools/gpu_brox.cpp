/*
  TODO - save the scaling information!
 */
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/gpu/gpumat.hpp"
#include "opencv2/gpu/gpu.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
//#include "flow_functions.h"

using namespace std;
using namespace cv;
using namespace cv::gpu;

// Some global variables for the optical flow
// Taken from brox_flow.cpp
const float alpha_ = 0.12; // 0.12
const float gamma_ = 5; // 5
const float scale_factor_ = 0.9;
const int inner_iterations_ = 3;
const int outer_iterations_ = 25; // 50
const int solver_iterations_ = 10; //20
const bool resize_img = false;
const float rfactor = 2.0;

string type2str(int type) {
  string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
  case CV_8U:  r = "8U"; break;
  case CV_8S:  r = "8S"; break;
  case CV_16U: r = "16U"; break;
  case CV_16S: r = "16S"; break;
  case CV_32S: r = "32S"; break;
  case CV_32F: r = "32F"; break;
  case CV_64F: r = "64F"; break;
  default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

int main(int argc, char **argv) {
  char output_file[255];
  char input_file[255];
  char input_dir[255];
  char output_dir[255];

  if (argc != 5) {
      cout << "Usage: gpu_brox <input_dir> <file with input video files> <output dir> <min-max file>" << endl;
    return 0;
  }
  strcpy(input_dir, argv[1]);
  strcpy(output_dir, argv[3]);
  fprintf(stderr, "output dir = %s\n", output_dir);
  long int num_frames = 0;
  Mat input_frame;  // Read video frames into a regular cv::Mat
  cv::gpu::GpuMat gpu_frame1, gpu_frame2; // Calculate dense optical flow using gpu on GpuMat's
  cv::gpu::GpuMat gpu_flowx, gpu_flowy; // To hold the optical flow... 
  cv::Mat gray_frame, flowx, flowy;
  std::vector<cv::Mat> gray_frames; // Vector of gray scale gpu frames from video. 
  std::vector<int> imwrite_params;
  imwrite_params.push_back(CV_IMWRITE_JPEG_QUALITY);
  imwrite_params.push_back(25);

  std::ifstream infile(argv[2]);
  std::ofstream outfile(argv[4]);
  cv::gpu::BroxOpticalFlow dflow(alpha_, gamma_, scale_factor_, inner_iterations_, outer_iterations_, solver_iterations_);
  while (infile >> input_file) {
    char video_file_path[255];
    sprintf(video_file_path, "%s/%s", input_dir, input_file);
    cout << "Processing " << video_file_path << endl;
    VideoCapture vid(video_file_path);
    if (!vid.isOpened()) {
      cout << "Could not open " << video_file_path << "; skipping..." << endl;
      continue;
    }

    // Go through and collect frames
    while (true) {
      vid >> input_frame;
      if (!input_frame.data) break;
      cv::Mat gray_frame;
      cv::cvtColor(input_frame, gray_frame, CV_BGR2GRAY); 
      cv::Mat gray_frame_float;
      gray_frame.convertTo(gray_frame_float, CV_32FC1, 1.0/255.0, 0);
      gray_frames.push_back(gray_frame_float);
    }
    num_frames += gray_frames.size();

    double range, minVal, maxVal; // Just to quickly get max values for scaling... 
    Point minLoc, maxLoc; //
    for (int i = 0; i < gray_frames.size() - 1; i++) {
      gpu_frame1.upload(gray_frames[i]);
      gpu_frame2.upload(gray_frames[i+1]);
      dflow(gpu_frame1, gpu_frame2, gpu_flowx, gpu_flowy);
      gpu_flowx.download(flowx);
      gpu_flowy.download(flowy);

      /*
      cv::namedWindow("image", WINDOW_AUTOSIZE);
      cv::imshow("image", flowx);
      cv::waitKey(30);
      */

      // We might as well mean subtract now instead of doing this on the fly in Caffe
      // since this is a per flow field mean...  
      cv::Scalar mean = cv::mean(flowx);
      flowx = flowx - mean;
      mean = cv::mean(flowy);
      flowy = flowy - mean;

      // Normalize each flow field to 0-255
      // 1. Subtract min value
      // 2. Scale by range = (max value - min value) / 255
      cv::minMaxLoc(flowx, &minVal, &maxVal, &minLoc, &maxLoc);
      outfile << input_file << "\t" << i  << "\t" << minVal << "\t" << maxVal << "\t"; 
      flowx = flowx - minVal;
      range = (maxVal - minVal)/255;
      flowx = flowx / range;
      cv::minMaxLoc(flowy, &minVal, &maxVal, &minLoc, &maxLoc);
      outfile << minVal << "\t" << maxVal << endl; 
      flowy = flowy - minVal;
      range = (maxVal - minVal)/255;
      flowy = flowy / range;
      // This is a 32FC1 image.

      sprintf(output_file, "%s/%s-flowx-%d.jpg", output_dir, input_file, i);
      cv::imwrite(output_file, flowx, imwrite_params);
      sprintf(output_file, "%s/%s-flowy-%d.jpg", output_dir, input_file, i);
      cv::imwrite(output_file, flowy, imwrite_params);

    }
    // Clear out the gray_frames vector. 
    gray_frames.clear();

  }
      
  return 0;
}


