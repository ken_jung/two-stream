/*
  Read in videos listed in file and print out information about them: 
  
 */
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/gpu/gpumat.hpp"
#include "opencv2/gpu/gpu.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;
using namespace cv;
using namespace cv::gpu;

int main(int argc, char **argv) {
  char input_file[255];

  if (argc != 5) {
    cout << "Usage: gpu_brox <file with input video files> <output file>" << endl;
    return 0;
  }
  Mat input_frame;  // Read video frames into a regular cv::Mat

  std::ifstream infile(argv[1]);
  std::ofstream outfile(argv[2]);
  outfile << "filename\tnum.frames\theight\twidth" << endl;
  int file_count = 0; 
  while (infile >> input_file) {
    cout << "Processing " << input_file << endl;
    VideoCapture vid(input_file);
    if (!vid.isOpened()) {
      cout << "Could not open " << input_file << "; skipping..." << endl;
      continue;
    }
    double fps = vid.get(CV_CAP_PROP_FPS);

    // Go through and collect frames
    int height = 0, width = 0, num_frames = 0;
    while (true) {
      vid >> input_frame;
      if (!input_frame.data) break;
      height = input_frame.rows;
      width = input_frame.cols;
      num_frames++;
    }
    
    outfile << input_file << "\t" << num_frames << "\t" << height << "\t" << width << "\t" << fps << endl;
    file_count++;
    if ((file_count % 100) == 0) 
      cerr << "Processed " << file_count << " files" << endl;
  }
      
  return 0;
}


