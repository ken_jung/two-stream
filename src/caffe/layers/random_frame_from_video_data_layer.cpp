#include <fstream>  // NOLINT(readability/streams)
#include <iostream>  // NOLINT(readability/streams)
#include <string>
#include <utility>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


#include "caffe/common.hpp"
#include "caffe/data_layers.hpp"
#include "caffe/layer.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/util/rng.hpp"

namespace caffe {

template <typename Dtype>
RandomFrameFromVideoDataLayer<Dtype>::~RandomFrameFromVideoDataLayer<Dtype>() {
  this->JoinPrefetchThread();
}

template <typename Dtype>
void RandomFrameFromVideoDataLayer<Dtype>::DataLayerSetUp(const vector<Blob<Dtype>*>& bottom,
							  const vector<Blob<Dtype>*>& top) {
  const int batch_size = this->layer_param_.random_frame_from_video_data_param().batch_size();
  const int height = this->layer_param_.random_frame_from_video_data_param().new_height();
  const int width  = this->layer_param_.random_frame_from_video_data_param().new_width();
  const int num_channels = 3; // BAD - hardwired in color images!
  CHECK((height == 0 && width == 0) ||
      (height > 0 && width > 0)) << "Current implementation requires "
      "height and width to be set at the same time.";

  // Read the file with filenames and labels
  const string& source = this->layer_param_.random_frame_from_video_data_param().source();
  LOG(INFO) << "Opening file " << source;
  std::ifstream infile(source.c_str());
  string filename;
  int label;
  while (infile >> filename >> label) {
    lines_.push_back(std::make_pair(filename, label));
  }

  // randomly shuffle data
  LOG(INFO) << "Shuffling data";
  const unsigned int prefetch_rng_seed = caffe_rng_rand();
  prefetch_rng_.reset(new Caffe::RNG(prefetch_rng_seed));
  ShuffleImages();
  LOG(INFO) << "A total of " << lines_.size() << " images.";
  lines_id_ = 0;

  // Initialize the top blob (i.e., the output from this layer)
  LOG(INFO) << "Reshaping top blobs";
  top[0]->Reshape(batch_size, num_channels, height, width);
  this->prefetch_data_.Reshape(batch_size, num_channels, height, width);

  LOG(INFO) << "output data size: " << top[0]->num() << ","
      << top[0]->channels() << "," << top[0]->height() << ","
      << top[0]->width();
  // label
  top[1]->Reshape(batch_size, 1, 1, 1);
  this->prefetch_label_.Reshape(batch_size, 1, 1, 1);

  // datum size
  //this->datum_channels_ = num_channels;
  //this->datum_height_ = height;
  //this->datum_width_ = width;
  //this->datum_size_ = num_channels * height * width;
}

template <typename Dtype>
void RandomFrameFromVideoDataLayer<Dtype>::ShuffleImages() {
  caffe::rng_t* prefetch_rng =
      static_cast<caffe::rng_t*>(prefetch_rng_->generator());
  shuffle(lines_.begin(), lines_.end(), prefetch_rng);
}

// This function is used to create a thread that prefetches the data.
template <typename Dtype>
void RandomFrameFromVideoDataLayer<Dtype>::InternalThreadEntry() {
  RandomFrameFromVideoDataParameter random_frame_from_video_data_param = this->layer_param_.random_frame_from_video_data_param();
  const int batch_size = random_frame_from_video_data_param.batch_size();
  const int height = random_frame_from_video_data_param.new_height();
  const int width = random_frame_from_video_data_param.new_width();
  const string mean_file = random_frame_from_video_data_param.mean_file();
  const int lines_size = lines_.size();
  CHECK(this->prefetch_data_.count());
  Dtype* top_data = this->prefetch_data_.mutable_cpu_data();
  Dtype* top_label = this->prefetch_label_.mutable_cpu_data();

  // Read in mean image file. This is an 8UC3 image. 
  cv::Mat mean_frame = cv::imread(mean_file);

  for (int item_id = 0; item_id < batch_size; ++item_id) {
    CHECK_GT(lines_size, lines_id_);
    cv::VideoCapture vid(lines_[lines_id_].first.c_str());
    if (!vid.isOpened()) {
      LOG(INFO) << "Could not open video file " << lines_[lines_id_].first << "; Aborting!...";
      exit(-1);
    }
    int num_frames = vid.get(CV_CAP_PROP_FRAME_COUNT);
    int offset = rand() % num_frames; // Stay away from boundary condition... 
    vid.set(CV_CAP_PROP_POS_FRAMES, offset);
    cv::Mat this_frame;
    vid >> this_frame;
    // This next part is because the number of frames that opencv returns is NOT RELIABLE!!!
    while (this_frame.rows == 0) {
      LOG(INFO) << "Got out of range frame " << offset << "/" << num_frames << " from " << lines_[lines_id_].first << "; retrying...";
      offset = rand() % num_frames;
      vid.set(CV_CAP_PROP_POS_FRAMES, offset);
      vid >> this_frame;
    }

    // Resize if necessary... 
    if (this_frame.rows != 240 || this_frame.cols != 320) {
      cv::Mat resized_frame;
      cv::Size size(320, 240);
      cv::resize(this_frame, resized_frame, size);
      this_frame = resized_frame;
    }

    // Randomly flip image with prob 0.5    
    int flipped_p = rand() % 2;
    cv::Mat flipped_frame;
    if (flipped_p) {
      cv::flip(this_frame, flipped_frame, 1);
      this_frame = flipped_frame;
    }
    CHECK_GE(this_frame.rows, height);
    CHECK_GE(this_frame.cols, width);
    const int h_offset = rand() % (this_frame.rows - height);
    const int w_offset = rand() % (this_frame.cols - width);
    CHECK_GE(h_offset, 0);
    CHECK_GE(w_offset, 0);
    for (int c = 0; c < 3; c++) { // BAD - hardwired in 3 channels... 
      for (int h = 0; h < height; ++h) {
	for (int w = 0; w < width; ++w) {
	  CHECK_LT(h+h_offset, this_frame.rows);
	  CHECK_LT(w+w_offset, this_frame.cols);
	  int offset = this->prefetch_data_.offset(item_id, c, h, w);
	  top_data[offset] = static_cast<Dtype>( this_frame.at<cv::Vec3b>(h+h_offset, w+w_offset)[c] ) - 
	    static_cast<Dtype>(mean_frame.at<cv::Vec3b>(h+h_offset, w+w_offset)[c]);
	}
      }
    }

    top_label[item_id] = lines_[lines_id_].second;

    // go to the next iter
    lines_id_++;
    if (lines_id_ >= lines_size) {
      // We have reached the end. Restart from the first.
      DLOG(INFO) << "Restarting data prefetching from start.";
      lines_id_ = 0;
      ShuffleImages();
    }
  }
}

INSTANTIATE_CLASS(RandomFrameFromVideoDataLayer);
REGISTER_LAYER_CLASS(RandomFrameFromVideoData);

}  // namespace caffe
