#include <fstream>  // NOLINT(readability/streams)
#include <iostream>  // NOLINT(readability/streams)
#include <string>
#include <utility>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


#include "caffe/common.hpp"
#include "caffe/data_layers.hpp"
#include "caffe/layer.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/util/rng.hpp"

namespace caffe {

template <typename Dtype>
TemporalNetSampleDataLayer<Dtype>::~TemporalNetSampleDataLayer<Dtype>() {
  this->JoinPrefetchThread();
}

template <typename Dtype>
void TemporalNetSampleDataLayer<Dtype>::DataLayerSetUp(const vector<Blob<Dtype>*>& bottom,
						       const vector<Blob<Dtype>*>& top) {
  //DEBUG
  LOG(ERROR) << "Setting up TemporalNetSampleDataLayer...";
  const int batch_size = 10; // We are always going to do ten variations per frame.  
  step_size_ = this->layer_param_.temporal_net_sample_data_param().step_size();
  num_flow_frames_ = this->layer_param_.temporal_net_sample_data_param().num_flow_frames();
  num_channels_ = 2*num_flow_frames_;  
  height_ = this->layer_param_.temporal_net_sample_data_param().new_height();
  width_  = this->layer_param_.temporal_net_sample_data_param().new_width();
  CHECK((height_ == 0 && width_ == 0) ||
      (height_ > 0 && width_ > 0)) << "Current implementation requires "
      "height and width to be set at the same time.";

  // Read the file with filenames and labels
  const string& source = this->layer_param_.temporal_net_sample_data_param().source();
  LOG(ERROR) << "Opening source file " << source;
  std::ifstream infile(source.c_str());
  string filename;
  int label;
  while (infile >> filename >> label) 
    lines_.push_back(std::make_pair(filename, label));
  LOG(ERROR) << "Read " << lines_.size() << " videos.";

  // Initialized counters for iterating through video files and frames in video files. 
  lines_id_ = 0;
  this->init_new_video();

  // Initialize the top blob (i.e., the output from this layer)
  top[0]->Reshape(batch_size, num_channels_, height_, width_);
  this->prefetch_data_.Reshape(batch_size, num_channels_, height_, width_);

  LOG(ERROR) << "output data size: " << top[0]->num() << ","
      << top[0]->channels() << "," << top[0]->height() << ","
      << top[0]->width();

  // label
  top[1]->Reshape(batch_size, 1, 1, 1);
  this->prefetch_label_.Reshape(batch_size, 1, 1, 1);

  // Need to set these even if we aren't using them b/c of some code in the base class. 
  //this->datum_channels_ = num_channels_;
  //this->datum_height_ = height_;
  //this->datum_width_ = width_;
  //this->datum_size_ = num_channels_ * height_ * width_;

}

template <typename Dtype>
void TemporalNetSampleDataLayer<Dtype>::ShuffleImages() {
  caffe::rng_t* prefetch_rng =
      static_cast<caffe::rng_t*>(prefetch_rng_->generator());
  shuffle(lines_.begin(), lines_.end(), prefetch_rng);
}

template <typename Dtype>
void TemporalNetSampleDataLayer<Dtype>::write_data(int h_offset, int w_offset, 
						 cv::Mat frame, int n, int c, 
						 float scale, float bias) {
  Dtype *input_data = this->prefetch_data_.mutable_cpu_data();
  for (int h = 0; h < height_; ++h) {
    for (int w = 0; w < width_; ++w) {
      int offset = this->prefetch_data_.offset(n, c, h, w);
      input_data[ offset ] = scale*static_cast<Dtype>( frame.at<uchar>(h_offset + h, w_offset + w) ) + bias;
    }
  }
}

template <typename Dtype>
void TemporalNetSampleDataLayer<Dtype>::init_new_video() {
  // Read in min and max values for the flow fields. 
  char scale_file[512];
  LOG(ERROR) << "Initializing for " << lines_[lines_id_].first;
  sprintf(scale_file, "%s-min-max.txt", lines_[lines_id_].first.c_str());
  std::ifstream scale_input(scale_file);
  float min_x, max_x, min_y, max_y;
  int min_i = 0;
  int max_i = 0;
  num_frames_in_video_ = 0;
  while (scale_input >> min_x >> max_x >> min_y >> max_y) {
    min_values_[min_i++] = min_x;
    min_values_[min_i++] = min_y;
    max_values_[max_i++] = max_x;
    max_values_[max_i++] = max_y;
    num_frames_in_video_++;
  }
  num_frames_in_video_--; // One less flow field than frames
  LOG(ERROR) << "Read " << num_frames_in_video_ << " from " << scale_file;
  frame_idx_ = 0;
  return;
}

// This function is used to create a thread that prefetches the data.
// Each "batch" for this layer is a set of ten crops/flips generated from 
// a single flow field stack of depth 2*num_flow_fields. 
template <typename Dtype>
void TemporalNetSampleDataLayer<Dtype>::InternalThreadEntry() {
  TemporalNetSampleDataParameter temporal_net_sample_data_param = this->layer_param_.temporal_net_sample_data_param();
  const int batch_size = 10;
  const int lines_size = lines_.size();
  CHECK(this->prefetch_data_.count());
  Dtype* top_label = this->prefetch_label_.mutable_cpu_data();

  if (lines_id_ < lines_size) {
    CHECK_GT(lines_size, lines_id_);
    if (frame_idx_ + num_flow_frames_ - 1 >= num_frames_in_video_) {
      // Advance to next video
      lines_id_++;
      if (lines_id_ == lines_size) {
	LOG(ERROR) << "Reached end of files";
	return;
      }
      this->init_new_video();      
    }
    
    // Read in the flow fields and stack up... 
    char file_buf[512]; 
    for (int c = 0; c < num_flow_frames_; c++) {
      int field_idx = frame_idx_ + c;

      // x-field
      sprintf(file_buf, "%s-flow/flowx-%d.jpg", lines_[lines_id_].first.c_str(), field_idx);
      cv::Mat x_field = cv::imread(file_buf, CV_LOAD_IMAGE_GRAYSCALE);
      cv::Mat x_flipped;
      cv::flip(x_field, x_flipped, 1);
      float x_scale = (max_values_[2*field_idx] - min_values_[2*field_idx]) / 255;
      float x_bias = min_values_[2*field_idx];

      // y-field
      sprintf(file_buf, "%s-flow/flowy-%d.jpg", lines_[lines_id_].first.c_str(), field_idx);
      cv::Mat y_field = cv::imread(file_buf, CV_LOAD_IMAGE_GRAYSCALE);
      cv::Mat y_flipped;
      cv::flip(y_field, y_flipped, 1);
      float y_scale = (max_values_[2*field_idx+1] - min_values_[2*field_idx+1]) / 255;
      float y_bias = min_values_[2*field_idx+1];

      // Write data; note that our outer iteration is over channels c, i.e., flow fields. 
      // Within this loop, we write to each crop/flip from the flow fields.  

      // Upper left
      write_data(0, 0, x_field, 0, 2*c, x_scale, x_bias);      
      write_data(0, 0, y_field, 0, 2*c + 1, y_scale, y_bias);    

      // Upper left, flipped
      write_data(0, 0, x_flipped, 1, 2*c, x_scale, x_bias);      
      write_data(0, 0, y_flipped, 1, 2*c + 1, y_scale, y_bias);    

      // Upper right
      write_data(0, x_field.cols - width_ - 1, 
		 x_field, 2, 2*c, x_scale, x_bias); 
      write_data(0, y_field.cols - width_ - 1, 
		 y_field, 2, 2*c + 1, y_scale, y_bias); 
      
      // Upper right, flipped
      write_data(0, x_flipped.cols - width_ - 1, 
		 x_flipped, 3, 2*c, x_scale, x_bias); 
      write_data(0, y_flipped.cols - width_ - 1, 
		 y_flipped, 3, 2*c + 1, y_scale, y_bias); 

      // Lower left
      write_data(x_field.rows - height_ - 1, 0, 
		 x_field, 4, 2*c, x_scale, x_bias); 
      write_data(y_field.rows - height_ - 1, 0, 
		 y_field, 4, 2*c + 1, y_scale, y_bias); 

      // Lower left, flipped
      write_data(x_flipped.rows - height_ - 1, 0, 
		 x_flipped, 5, 2*c, x_scale, x_bias); 
      write_data(y_flipped.rows - height_ - 1, 0, 
		 y_flipped, 5, 2*c + 1, y_scale, y_bias); 

      // Lower right
      write_data(x_field.rows - height_ - 1, 
		 x_field.cols - width_ - 1, 
		 x_field, 6, 2*c, x_scale, x_bias);  
      write_data(y_field.rows - height_ - 1, 
		 y_field.cols - width_ - 1, 
		 y_field, 6, 2*c + 1, y_scale, y_bias);  

      // Lower right, flipped
      write_data(x_flipped.rows - height_ - 1, 
		 x_flipped.cols - width_ - 1, 
		 x_flipped, 7, 2*c, x_scale, x_bias);  
      write_data(y_flipped.rows - height_ - 1, 
		 y_flipped.cols - width_ - 1, 
		 y_flipped, 7, 2*c + 1, y_scale, y_bias);  

      // Center
      write_data((x_field.rows - height_)/2, 
		 (x_field.cols - width_)/2, 
		 x_field, 8, 2*c, x_scale, x_bias); 
      write_data((y_field.rows - height_)/2, 
		 (y_field.cols - width_)/2, 
		 y_field, 8, 2*c + 1, y_scale, y_bias); 

      // Center, flipped
      write_data((x_flipped.rows - height_)/2, 
		 (x_flipped.cols - width_)/2, 
		 x_flipped, 9, 2*c, x_scale, x_bias); 
      write_data((y_flipped.rows - height_)/2, 
		 (y_flipped.cols - width_)/2, 
		 y_flipped, 9, 2*c + 1, y_scale, y_bias); 

    }

    for (int i = 0; i << batch_size; i++) 
      top_label[i] = lines_[lines_id_].second;

    frame_idx_ += step_size_;

  } else {
    LOG(ERROR) << "Reached end of input files!";
  }
}

INSTANTIATE_CLASS(TemporalNetSampleDataLayer);
REGISTER_LAYER_CLASS(TemporalNetSampleData);
}  // namespace caffe



