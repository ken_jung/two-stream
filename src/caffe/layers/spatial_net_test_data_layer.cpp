#include <fstream>  // NOLINT(readability/streams)
#include <iostream>  // NOLINT(readability/streams)
#include <string>
#include <utility>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


#include "caffe/common.hpp"
#include "caffe/data_layers.hpp"
#include "caffe/layer.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/util/rng.hpp"

// TODO - rewrite so that we are writing directly to the blob.  

namespace caffe {

template <typename Dtype>
TestSpatialNetDataLayer<Dtype>::~TestSpatialNetDataLayer<Dtype>() {
  this->JoinPrefetchThread();
}

template <typename Dtype>
void TestSpatialNetDataLayer<Dtype>::DataLayerSetUp(const vector<Blob<Dtype>*>& bottom,
						    const vector<Blob<Dtype>*>& top) {
  const int batch_size = 10; // We are always going to do ten variations per frame.  
  num_frames_per_video_ = this->layer_param_.test_spatial_net_data_param().num_frames_per_video();  // How many frames per video do we do?  
  LOG(ERROR) << "Using " << num_frames_per_video_ << " frames per video";
  height_ = this->layer_param_.test_spatial_net_data_param().new_height();
  width_  = this->layer_param_.test_spatial_net_data_param().new_width();
  const string mean_file = this->layer_param_.test_spatial_net_data_param().mean_file();

  LOG(ERROR) << "height and width: " << height_ << ", " << width_;
  LOG(ERROR) << "mean file: " << mean_file;
  const int num_channels = 3; // BAD - hardwired in color images!
  CHECK((height_ == 0 && width_ == 0) ||
      (height_ > 0 && width_ > 0)) << "Current implementation requires "
      "height and width to be set at the same time.";

  mean_frame_ = cv::imread(mean_file);
  cv::flip(mean_frame_, flipped_mean_, 1);

  // Read the file with filenames and labels
  const string& source = this->layer_param_.test_spatial_net_data_param().source();
  LOG(ERROR) << "Opening file " << source;
  std::ifstream infile(source.c_str());
  string filename;
  int label;
  while (infile >> filename >> label) 
    lines_.push_back(std::make_pair(filename, label));
  LOG(ERROR) << "A total of " << lines_.size() << " images.";

  // Initialized counters for iterating through video files and frames in video files. 
  lines_id_ = 0;
  frame_num_ = 0;

  // Get number of frames in the first video so we can setup the first iteration.
  cv::VideoCapture vid(lines_[lines_id_].first.c_str());
  if (!vid.isOpened()) {
    LOG(ERROR) << "Could not open video file " << lines_[lines_id_].first << "; Aborting!...";
    exit(-1);
  }
  int num_frames_in_video = 0;
  cv::Mat this_frame;
  while (true) {
    vid >> this_frame;
    if (!this_frame.data) break;
    num_frames_in_video++;
  }
  step_size_ = num_frames_in_video / num_frames_per_video_;
  frame_idx_ = (num_frames_in_video % num_frames_per_video_) / 2;
  frame_num_ = 0;
  //LOG(ERROR) << "\tRead " << num_frames_in_video << " frames; using step size " << step_size_ << " from " << frame_idx_;

  // Initialize the top blob (i.e., the output from this layer)
  LOG(ERROR) << "Reshaping top blobs";
  (top)[0]->Reshape(batch_size, num_channels, height_, width_);
  this->prefetch_data_.Reshape(batch_size, num_channels, height_, width_);

  LOG(ERROR) << "output data size: " << (top)[0]->num() << ","
      << (top)[0]->channels() << "," << (top)[0]->height() << ","
      << (top)[0]->width();

  // label
  (top)[1]->Reshape(batch_size, 1, 1, 1);
  this->prefetch_label_.Reshape(batch_size, 1, 1, 1);

  // Need to set these even if we aren't using them b/c of some code in the base class. 
  //this->datum_channels_ = num_channels;
  //this->datum_height_ = height_;
  //this->datum_width_ = width_;
  //this->datum_size_ = num_channels * height_ * width_;
}

template <typename Dtype>
void TestSpatialNetDataLayer<Dtype>::ShuffleImages() {
  caffe::rng_t* prefetch_rng =
      static_cast<caffe::rng_t*>(prefetch_rng_->generator());
  shuffle(lines_.begin(), lines_.end(), prefetch_rng);
}

template <typename Dtype>
void TestSpatialNetDataLayer<Dtype>::write_data(int h_offset, int w_offset, 
						cv::Mat frame, cv::Mat mean_frame, 
						int n) {
  Dtype *input_data = this->prefetch_data_.mutable_cpu_data();
  for (int c = 0; c < 3; c++) { 
    for (int h = 0; h < height_; ++h) {
      for (int w = 0; w < width_; ++w) {
	int offset = this->prefetch_data_.offset(n, c, h, w);
	input_data[ offset ] = static_cast<Dtype>(frame.at<cv::Vec3b>(h,w)[c]) - 
	  static_cast<Dtype>(mean_frame.at<cv::Vec3b>(h,w)[c]);
      }
    }
  }
}


// This function is used to create a thread that prefetches the data.
template <typename Dtype>
void TestSpatialNetDataLayer<Dtype>::InternalThreadEntry() {
  TestSpatialNetDataParameter test_spatial_net_data_param = this->layer_param_.test_spatial_net_data_param();
  const int batch_size = 10;
  const int lines_size = lines_.size();
  CHECK(this->prefetch_data_.count());
  Dtype* top_label = this->prefetch_label_.mutable_cpu_data();

  // Read in mean image file. This is an 8UC3 image. 

  if (lines_id_ < lines_size) {
    CHECK_GT(lines_size, lines_id_);

    if (frame_num_ == num_frames_per_video_) {
      // Advance to next video
      lines_id_++;
      if (lines_id_ == lines_size) {
	LOG(ERROR) << "Got to end of files...";
	return;
      }
      LOG(ERROR) << "Starting on " << lines_[lines_id_].first.c_str();
      
      // Get number of frames in the new video
      cv::VideoCapture vid(lines_[lines_id_].first.c_str());
      if (!vid.isOpened()) {
	LOG(ERROR) << "Could not open video file " << lines_[lines_id_].first << "; Aborting!...";
	exit(-1);
      }
      int num_frames_in_video = 0;
      cv::Mat this_frame;
      while (true) {
	vid >> this_frame;
	if (!this_frame.data) break;
	num_frames_in_video++;
      }
      
      // Set up vars for this video
      step_size_ = num_frames_in_video / num_frames_per_video_;
      frame_idx_ = (num_frames_in_video % num_frames_per_video_) / 2;
      frame_num_ = 0;
      LOG(ERROR) << "\tRead " << num_frames_in_video << " frames; using step size " << step_size_ << " from " << frame_idx_;
    }
    
    // Read in the video capture and advance to the right frame.
    cv::VideoCapture vid(lines_[lines_id_].first.c_str());
    if (!vid.isOpened()) {
      LOG(ERROR) << "Could not open video file " << lines_[lines_id_].first << "; Aborting!...";
      exit(-1);
    }
    vid.set(CV_CAP_PROP_POS_FRAMES, frame_idx_);
    cv::Mat orig_frame;
    vid >> orig_frame;
    cv::Mat flipped_frame;
    cv::flip(orig_frame, flipped_frame, 1);

    write_data(0, 0, orig_frame, mean_frame_, 0);       // Upper left
    write_data(0, 0, flipped_frame, flipped_mean_, 1);    // Upper left of flipped
    write_data(0, orig_frame.cols - width_ - 1, 
	       orig_frame, mean_frame_, 2); // Upper right
    write_data(0, orig_frame.cols - width_ - 1, 
	       flipped_frame, flipped_mean_, 3); // Upper right of flipped
    write_data(orig_frame.rows - height_ - 1, 0, 
	       orig_frame, mean_frame_, 4); // Lower left
    write_data(orig_frame.rows - height_ - 1, 0, 
	       flipped_frame, flipped_mean_, 5); // Lower left of flipped
    write_data(orig_frame.rows - height_ - 1, 
	       orig_frame.cols - width_ - 1, 
	       orig_frame, mean_frame_, 6);  // Lower right
    write_data(orig_frame.rows - height_ - 1, 
	       orig_frame.cols - width_ - 1, 
	       flipped_frame, flipped_mean_, 7);  // Lower right
    write_data((orig_frame.rows - height_)/2, 
	       (orig_frame.cols - width_)/2, 
	       orig_frame, mean_frame_, 8); // Center
    write_data((flipped_frame.rows - height_)/2, 
	       (flipped_frame.cols - width_)/2, 
	       flipped_frame, flipped_mean_, 8); // Center, flipped
    for (int i = 0; i << batch_size; i++) 
      top_label[i] = lines_[lines_id_].second;

    // Advance frame_num_ and frame_idx_
    frame_num_++;
    frame_idx_ += step_size_;

  } else {
    LOG(ERROR) << "Reached end of input files!";
  }
}

INSTANTIATE_CLASS(TestSpatialNetDataLayer);
REGISTER_LAYER_CLASS(TestSpatialNetData);
}  // namespace caffe



