#include <fstream>  // NOLINT(readability/streams)
#include <iostream>  // NOLINT(readability/streams)
#include <string>
#include <utility>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/imgproc.hpp>

#include "caffe/common.hpp"
#include "caffe/data_layers.hpp"
#include "caffe/layer.hpp"
#include "caffe/util/benchmark.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/util/rng.hpp"


namespace caffe {

template <typename Dtype>
OpticalFlowStackDataLayer<Dtype>::~OpticalFlowStackDataLayer<Dtype>() {
  this->JoinPrefetchThread();
}

template <typename Dtype>
void OpticalFlowStackDataLayer<Dtype>::DataLayerSetUp(const vector<Blob<Dtype>*>& bottom,
						      const vector<Blob<Dtype>*>& top) {
  DLOG(INFO) << "Optical Flow Stack Data Layer Setup called...  Why the fuck isn't this called?!";
  const int batch_size = this->layer_param_.optical_flow_stack_data_param().batch_size();
  const int height = this->layer_param_.optical_flow_stack_data_param().new_height();
  const int width  = this->layer_param_.optical_flow_stack_data_param().new_width();
  CHECK((height == 0 && width == 0) ||
      (height > 0 && width > 0)) << "Current implementation requires "
      "height and width to be set at the same time.";
  const int num_flow_frames = this->layer_param_.optical_flow_stack_data_param().num_flow_frames();
  const int num_channels = 2*num_flow_frames;
  LOG(INFO) << "Using " << num_flow_frames << " frames for " << num_channels << " channels";

  // Read the file with filenames and labels
  const string& source = this->layer_param_.optical_flow_stack_data_param().source();
  DLOG(INFO) << "Opening file " << source;
  std::ifstream infile(source.c_str());
  string filename;
  int label;
  while (infile >> filename >> label) {
    lines_.push_back(std::make_pair(filename, label));
  }

  // randomly shuffle data
  LOG(INFO) << "Shuffling data";
  const unsigned int prefetch_rng_seed = caffe_rng_rand();
  prefetch_rng_.reset(new Caffe::RNG(prefetch_rng_seed));
  ShuffleImages();
  LOG(INFO) << "A total of " << lines_.size() << " images.";
  lines_id_ = 0;

  // Initialize the top blob (i.e., the output from this layer)
  LOG(INFO) << "Reshaping top blobs";
  top[0]->Reshape(batch_size, num_channels, height, width);
  this->prefetch_data_.Reshape(batch_size, num_channels, height, width);

  LOG(INFO) << "output data size: " << top[0]->num() << ","
      << top[0]->channels() << "," << top[0]->height() << ","
      << top[0]->width();
  // label
  top[1]->Reshape(batch_size, 1, 1, 1);
  this->prefetch_label_.Reshape(batch_size, 1, 1, 1);

  // datum size
  //this->datum_channels_ = num_channels;
  //this->datum_height_ = height;
  //this->datum_width_ = width;
  //this->datum_size_ = num_channels * height * width;
}

template <typename Dtype>
void OpticalFlowStackDataLayer<Dtype>::ShuffleImages() {
  caffe::rng_t* prefetch_rng =
      static_cast<caffe::rng_t*>(prefetch_rng_->generator());
  shuffle(lines_.begin(), lines_.end(), prefetch_rng);
}

// This function is used to create a thread that prefetches the data.
template <typename Dtype>
void OpticalFlowStackDataLayer<Dtype>::InternalThreadEntry() {
  //LOG(INFO) << "Prefetching data...";
  OpticalFlowStackDataParameter optical_flow_stack_data_param = this->layer_param_.optical_flow_stack_data_param();
  const int batch_size = optical_flow_stack_data_param.batch_size();
  const int height = optical_flow_stack_data_param.new_height();
  const int width = optical_flow_stack_data_param.new_width();
  const int num_flow_frames = optical_flow_stack_data_param.num_flow_frames();
  const int lines_size = lines_.size();
  CHECK(this->prefetch_data_.count());
  Dtype* top_data = this->prefetch_data_.mutable_cpu_data();
  Dtype* top_label = this->prefetch_label_.mutable_cpu_data();

  // Let's see what happens now - these are statically allocated on the program stack, and 
  // are large enough that it'll work at least for UCF101.  
  float min_values[100000];
  float max_values[100000];

  for (int item_id = 0; item_id < batch_size; ++item_id) {
    CHECK_GT(lines_size, lines_id_);

    // Read in min and max values for the flow fields. 
    char scale_file[512];
    sprintf(scale_file, "%s-min-max.txt", lines_[lines_id_].first.c_str());
    std::ifstream scale_input(scale_file);
    float min_x, max_x, min_y, max_y;
    int min_i = 0;
    int max_i = 0;
    int total_num_frames = 0;
    while (scale_input >> min_x >> max_x >> min_y >> max_y) {
      min_values[min_i++] = min_x;
      min_values[min_i++] = min_y;
      max_values[max_i++] = max_x;
      max_values[max_i++] = max_y;
      total_num_frames++;
    }
    const int start_frame = rand() % (total_num_frames - num_flow_frames);

    // Read into top_data... 
    // First, read in first frame to set random crop.
    char file_buf[512];
    string base_filename = lines_[lines_id_].first;
    sprintf(file_buf, "%s-flowx-0.jpg", base_filename.c_str());
    cv::Mat cv_img_first = cv::imread(file_buf, CV_LOAD_IMAGE_GRAYSCALE);
    // How do we see this such that every call gets something different?  time(NULL) is no good... 
    const int h_offset = rand() % (cv_img_first.rows - height);
    const int w_offset = rand() % (cv_img_first.cols - width);

    // Decide whether or not to flip the fields horizontally
    int flip_p = rand() % 2;

    // Now read in successive frames of x and y flows, starting with start_frame... 
    for (int c = 0; c < num_flow_frames; c++) {
      int frame_idx = c + start_frame;
      // Read in x field, rescale, write top blob
      sprintf(file_buf, "%s-flowx-%d.jpg", base_filename.c_str(), frame_idx);
      cv::Mat x_img = cv::imread(file_buf, CV_LOAD_IMAGE_GRAYSCALE);
      if (flip_p) {
	cv::Mat flipped_img;
	cv::flip(x_img, flipped_img, 1);
	x_img = flipped_img;
      }
      float x_scale = (max_values[2*frame_idx] - min_values[2*frame_idx]) / 255;
      for (int h = 0; h < height; ++h) {
	for (int w = 0; w < width; ++w) {
	  Dtype value = static_cast<Dtype>( x_img.at<uchar>(h + h_offset, w + w_offset) ); 
	  int offset = this->prefetch_data_.offset(item_id, 2*c, h, w);
	  top_data[ offset ] = x_scale*value + min_values[2*frame_idx];
	}
      }

      // Read in y field and write to top blob
      sprintf(file_buf, "%s-flowy-%d.jpg", base_filename.c_str(), frame_idx);
      cv::Mat y_img = cv::imread(file_buf, CV_LOAD_IMAGE_GRAYSCALE);
      if (flip_p) {
	cv::Mat flipped_img;
	cv::flip(y_img, flipped_img, 1);
	y_img = flipped_img;
      }
      float y_scale = (max_values[2*frame_idx+1] - min_values[2*frame_idx+1]) / 255;
      for (int h = 0; h < height; ++h) {
	for (int w = 0; w < width; ++w) {
	  Dtype value = static_cast<Dtype>( y_img.at<uchar>(h + h_offset, w + w_offset) );
	  int offset = this->prefetch_data_.offset(item_id, 2*c+1, h, w);
	  top_data[ offset ] = y_scale*value + min_values[2*frame_idx + 1];
	}
      }
    }

    top_label[item_id] = lines_[lines_id_].second;

    // go to the next iter
    lines_id_++;
    if (lines_id_ >= lines_size) {
      // We have reached the end. Restart from the first.
      DLOG(INFO) << "Restarting data prefetching from start.";
      lines_id_ = 0;
      if (this->layer_param_.optical_flow_stack_data_param().shuffle()) {
        ShuffleImages();
      }
    }
  }

}

INSTANTIATE_CLASS(OpticalFlowStackDataLayer);
REGISTER_LAYER_CLASS(OpticalFlowStackData);

}  // namespace caffe
